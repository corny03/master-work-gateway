package com.zajac.adam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.InternalResourceView;

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
@RestController
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    /**
     * Method redirect to view stored in this server
     *
     * @return View
     */
    @GetMapping({
            "/user",
            "/image/**",
            "/instagram",
            "/link-image",
            "/link-image/**",
            "/simpleTester",
            "/uploader",
            "/about"})
    public View redirect() {
        return new InternalResourceView("/index.html");
    }

    /**
     * Method contain endpoints to available images filters
     * @return available images filters
     */
    @GetMapping("/filters-endpoints")
    public String[] filters() {
        return new String[]{
                "/binarized-img/",
                "/color-binarized-img/",
                "/red-binarized-img/",
                "/green-binarized-img/",
                "/blue-binarized-img/",
                "/redblue-bin-img/",
                "/redgreen-bin-img/",
                "/greenblue-bin-img/",
                "/random-one-img/"};
    }
}
