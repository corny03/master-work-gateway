webpackJsonp(["main"], {

    /***/
    "../../../../../src/$$_gendir lazy recursive": /***/ (function (module, exports) {

        function webpackEmptyAsyncContext(req) {
            // Here Promise.resolve().then() is used instead of new Promise() to prevent
            // uncatched exception popping up in devtools
            return Promise.resolve().then(function () {
                throw new Error("Cannot find module '" + req + "'.");
            });
        }

        webpackEmptyAsyncContext.keys = function () {
            return [];
        };
        webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
        module.exports = webpackEmptyAsyncContext;
        webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

        /***/
    }),

    /***/
    "../../../../../src/app/address-settings.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return AddressSettings;
        });
        /**
         * Created by zajac on 24.06.2017.
         */
        var AddressSettings = (function () {
            function AddressSettings() {
            }

            return AddressSettings;
        }());

        AddressSettings.DOMAIN_URL = 'http://localhost:8899';
        AddressSettings.BACKEND_REST_ENDPOINT = '';
        AddressSettings.INSTAGRAM_IMAGES_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/oauth/user/images-links';
        AddressSettings.UPLOAD_FILE_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/dispatcher/uploadMultipartFile';
        AddressSettings.UPLOAD_URL_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/dispatcher/uploadFromLink';
        AddressSettings.IMAGE_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/img/';
        AddressSettings.IMAGE_DETAIL_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/image/';
        AddressSettings.BINARIZED_IMAGE_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/binarized-img/';
        AddressSettings.COLOR_BINARIZED_IMAGE_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/color-binarized-img/';
        AddressSettings.LAST_12_IDS_IMAGES_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/img/lasts12';
        AddressSettings.MY_IMAGES_IDS = AddressSettings.BACKEND_REST_ENDPOINT + '/img/by-user/';
        AddressSettings.IMAGES_FILTERS_ADDRESSES_URL = AddressSettings.BACKEND_REST_ENDPOINT + '/filters-endpoints';
//# sourceMappingURL=address-settings.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/app-routing.module.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return AppRoutingModule;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2__tester_simple_test_component__ = __webpack_require__("../../../../../src/app/tester/simple-test.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_3__static_welcome_component__ = __webpack_require__("../../../../../src/app/static/welcome.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_4__uploader_uploader_component__ = __webpack_require__("../../../../../src/app/uploader/uploader.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_5__uploader_image_component__ = __webpack_require__("../../../../../src/app/uploader/image.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_6__user_users_component__ = __webpack_require__("../../../../../src/app/user/users.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_7__instagram_instagram_photos_component__ = __webpack_require__("../../../../../src/app/instagram/instagram-photos.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_8__uploader_image_from_link_uploader_component__ = __webpack_require__("../../../../../src/app/uploader/image-from-link-uploader.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_9__user_uploaded_by_user_component__ = __webpack_require__("../../../../../src/app/user/uploaded-by-user.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_10__navigation_component__ = __webpack_require__("../../../../../src/app/navigation.component.ts");
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };


        var routes = [
            {
                path: 'about',
                component: __WEBPACK_IMPORTED_MODULE_3__static_welcome_component__["a" /* WelcomeComponent */]
            },
            {
                path: 'main',
                component: __WEBPACK_IMPORTED_MODULE_10__navigation_component__["a" /* NavigationComponent */]
            },
            {
                path: 'simpleTester',
                component: __WEBPACK_IMPORTED_MODULE_2__tester_simple_test_component__["a" /* SimpleTestComponent */]
            },
            {
                path: 'uploader',
                component: __WEBPACK_IMPORTED_MODULE_4__uploader_uploader_component__["a" /* UploaderComponent */]
            },
            {
                path: 'image/:id',
                component: __WEBPACK_IMPORTED_MODULE_5__uploader_image_component__["a" /* ImageComponent */]
            },
            {path: 'user', component: __WEBPACK_IMPORTED_MODULE_6__user_users_component__["a" /* UserComponent */]},
            {
                path: 'instagram',
                component: __WEBPACK_IMPORTED_MODULE_7__instagram_instagram_photos_component__["a" /* InstagramPhotosComponent */]
            },
            {
                path: 'link-image/:imgUrl',
                component: __WEBPACK_IMPORTED_MODULE_8__uploader_image_from_link_uploader_component__["a" /* ImageFromLinkUploaderComponent */]
            },
            {
                path: 'uploaded-by-user',
                component: __WEBPACK_IMPORTED_MODULE_9__user_uploaded_by_user_component__["a" /* UploadedByUserComponent */]
            }
        ];
        var AppRoutingModule = (function () {
            function AppRoutingModule() {
            }

            return AppRoutingModule;
        }());
        AppRoutingModule = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["M" /* NgModule */])({
                imports: [__WEBPACK_IMPORTED_MODULE_0__angular_router__["c" /* RouterModule */].forRoot(routes)],
                exports: [__WEBPACK_IMPORTED_MODULE_0__angular_router__["c" /* RouterModule */]]
            })
        ], AppRoutingModule);

//# sourceMappingURL=app-routing.module.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/app.component.css": /***/ (function (module, exports, __webpack_require__) {

        exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
        exports.push([module.i, "\r\na, .big-letter {\r\n  font-size: 24px;\r\n}\r\n\r\nh1 {\r\n  font-size: 72px;\r\n  /*font-weight: bold;*/\r\n  letter-spacing: 0;\r\n}\r\n\r\nh4 {\r\n  font-size: 17px;\r\n  font-weight: inherit;\r\n}\r\n\r\n.bg {\r\n  color: #FFFFFF;\r\n  /* The image used */\r\n  background-image: url(" + __webpack_require__("../../../../../src/assets/background.jpg") + ");\r\n  /* Full height */\r\n  height: 100vh;\r\n  padding-top: 14vw;\r\n  text-align: center;\r\n  /* Center and scale the image nicely */\r\n  background-position: center;\r\n  background-repeat: no-repeat;\r\n  background-size: cover;\r\n}\r\n\r\n.button {\r\n  background-color: #47B29E;\r\n  color: #FFFFFF;\r\n  padding: 4vh 10vw 4vh 10vw;\r\n  margin: 8vh;\r\n}\r\n\r\n", ""]);

// exports


        /*** EXPORTS FROM exports-loader ***/
        module.exports = module.exports.toString();

        /***/
    }),

    /***/
    "../../../../../src/app/app.component.html": /***/ (function (module, exports) {

        module.exports = "<body *ngIf=\"router.url ==='/'\">\n<div class=\"bg\">\n  <h1>ONEWEEKPHOTO</h1>\n  <h4>Podziel się chwilą. Twoi znajomi mają tylko tydzień na obejrzenie.</h4>\n  <br/>\n  <br/>\n  <br/>\n  <br/>\n  <br/>\n  <br/>\n  <a class=\"button\" href=\"/uploader\">PRZEŚLIJ</a>\n  <span class=\"big-letter\">LUB</span>\n  <a class=\"button\" href=\"/user\">ZALOGUJ</a>\n</div>\n\n</body>\n\n<app-navigation *ngIf=\"router.url !=='/'\"></app-navigation>\n\n"

        /***/
    }),

    /***/
    "../../../../../src/app/app.component.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return AppComponent;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__user_users_service__ = __webpack_require__("../../../../../src/app/user/users.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2__user_Principal__ = __webpack_require__("../../../../../src/app/user/Principal.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };
        var __metadata = (this && this.__metadata) || function (k, v) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
            };


        var AppComponent = (function () {
            function AppComponent(userService, router) {
                this.userService = userService;
                this.router = router;
                this.title = 'OneWeekPhoto™';
                this.principal = new __WEBPACK_IMPORTED_MODULE_2__user_Principal__["a" /* Principal */](false, 'undefined', 'undefined', 'undefined');
            }

            AppComponent.prototype.ngOnInit = function () {
                this.getUser();
            };
            AppComponent.prototype.getUser = function () {
                var _this = this;
                this.userService.getUser().subscribe(function (response) {
                    if (response.status <= 300) {
                        _this.principal = response.json();
                    }
                }, function (error) {
                    console.log('Unauthorized');
                });
            };
            return AppComponent;
        }());
        AppComponent = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
                selector: 'app-root',
                template: __webpack_require__("../../../../../src/app/app.component.html"),
                styles: [__webpack_require__("../../../../../src/app/app.component.css")]
            }),
            __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__user_users_service__["a" /* UsersService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__user_users_service__["a" /* UsersService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === "function" && _b || Object])
        ], AppComponent);

        var _a, _b;
//# sourceMappingURL=app.component.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/app.module.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return AppModule;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_4_angular2_masonry__ = __webpack_require__("../../../../angular2-masonry/index.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_6__app_routing_module__ = __webpack_require__("../../../../../src/app/app-routing.module.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_7__tester_simple_test_component__ = __webpack_require__("../../../../../src/app/tester/simple-test.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_8__tester_test_service__ = __webpack_require__("../../../../../src/app/tester/test.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_9__static_welcome_component__ = __webpack_require__("../../../../../src/app/static/welcome.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_10__uploader_uploader_component__ = __webpack_require__("../../../../../src/app/uploader/uploader.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_11__uploader_image_service__ = __webpack_require__("../../../../../src/app/uploader/image.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_12__uploader_image_component__ = __webpack_require__("../../../../../src/app/uploader/image.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_13__user_users_component__ = __webpack_require__("../../../../../src/app/user/users.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_14__instagram_instagram_photos_component__ = __webpack_require__("../../../../../src/app/instagram/instagram-photos.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_15__user_users_service__ = __webpack_require__("../../../../../src/app/user/users.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_16__instagram_instagram_service__ = __webpack_require__("../../../../../src/app/instagram/instagram.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_17__uploader_image_from_link_uploader_component__ = __webpack_require__("../../../../../src/app/uploader/image-from-link-uploader.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_18__user_uploaded_by_user_component__ = __webpack_require__("../../../../../src/app/user/uploaded-by-user.component.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_19__navigation_component__ = __webpack_require__("../../../../../src/app/navigation.component.ts");
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };


        var AppModule = (function () {
            function AppModule() {
            }

            return AppModule;
        }());
        AppModule = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["M" /* NgModule */])({
                declarations: [
                    __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
                    __WEBPACK_IMPORTED_MODULE_7__tester_simple_test_component__["a" /* SimpleTestComponent */],
                    __WEBPACK_IMPORTED_MODULE_9__static_welcome_component__["a" /* WelcomeComponent */],
                    __WEBPACK_IMPORTED_MODULE_10__uploader_uploader_component__["a" /* UploaderComponent */],
                    __WEBPACK_IMPORTED_MODULE_12__uploader_image_component__["a" /* ImageComponent */],
                    __WEBPACK_IMPORTED_MODULE_13__user_users_component__["a" /* UserComponent */],
                    __WEBPACK_IMPORTED_MODULE_14__instagram_instagram_photos_component__["a" /* InstagramPhotosComponent */],
                    __WEBPACK_IMPORTED_MODULE_17__uploader_image_from_link_uploader_component__["a" /* ImageFromLinkUploaderComponent */],
                    __WEBPACK_IMPORTED_MODULE_18__user_uploaded_by_user_component__["a" /* UploadedByUserComponent */],
                    __WEBPACK_IMPORTED_MODULE_19__navigation_component__["a" /* NavigationComponent */]
                ],
                imports: [
                    __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                    __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                    __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
                    __WEBPACK_IMPORTED_MODULE_6__app_routing_module__["a" /* AppRoutingModule */],
                    __WEBPACK_IMPORTED_MODULE_4_angular2_masonry__["a" /* MasonryModule */]
                ],
                providers: [__WEBPACK_IMPORTED_MODULE_8__tester_test_service__["a" /* TestService */], __WEBPACK_IMPORTED_MODULE_11__uploader_image_service__["a" /* ImageService */], __WEBPACK_IMPORTED_MODULE_15__user_users_service__["a" /* UsersService */], __WEBPACK_IMPORTED_MODULE_16__instagram_instagram_service__["a" /* InstagramService */]],
                bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
            })
        ], AppModule);

//# sourceMappingURL=app.module.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/instagram/instagram-photos.component.html": /***/ (function (module, exports) {

        module.exports = "<div class=\"row col-md-12\">\r\n  <h2>Zdjęcia z profilu Instagram</h2>\r\n  <p>Kliknij na wybranym zdjęciu by go wysłać do serwisu</p>\r\n  <hr/>\r\n  <br/>\r\n</div>\r\n<div class=\"row col-md-12\">\r\n  <masonry>\r\n    <div class=\"grid\" data-masonry='{ \"itemSelector\": \".grid-item\", \"columnWidth\": 0}'>\r\n      <div class=\"grid-sizer\"></div>\r\n      <masonry-brick class=\"brick grid-item\" *ngFor=\"let imageUrl of imagesUrls\">\r\n        <img class=\"img-rounded\" src=\"{{imageUrl}}\" (click)=\"openSingleImage(imageUrl)\"\r\n             routerLinkActive=\"active\"/>\r\n      </masonry-brick>\r\n    </div>\r\n  </masonry>\r\n</div>\r\n\r\n<!--<div class=\"row\" *ngFor=\"let url of imageUrls ; let i = index\">-->\r\n\r\n<!--<div class=\"col-sm-3\">-->\r\n<!--<a (click)=\"openSingleImage(url[0])\">-->\r\n<!--<img src=\"{{url[0]}}\" class=\"img-responsive img-rounded\" style=\"max-height:250px; margin: auto;\">-->\r\n<!--</a>-->\r\n<!--</div>-->\r\n\r\n<!--<div class=\"col-sm-3 \">-->\r\n<!--<a (click)=\"openSingleImage(url[1])\" routerLinkActive=\"active\">-->\r\n<!--<img src=\"{{ url[1]}}\" class=\"img-responsive img-rounded\" style=\"max-height:250px; margin: auto;\">-->\r\n<!--</a>-->\r\n<!--</div>-->\r\n\r\n<!--<div class=\"col-sm-3\">-->\r\n<!--<a (click)=\"openSingleImage(url[2])\" routerLinkActive=\"active\">-->\r\n<!--<img src=\"{{url[2]}}\" class=\"img-responsive img-rounded\" style=\"max-height:250px; margin: auto;\">-->\r\n<!--</a>-->\r\n<!--</div>-->\r\n\r\n<!--<div class=\"col-sm-3\">-->\r\n<!--<a (click)=\"openSingleImage(url[3])\" routerLinkActive=\"active\">-->\r\n<!--<img src=\"{{url[3]}}\" class=\"img-responsive img-rounded\" style=\"max-height:250px; margin: auto;\">-->\r\n<!--</a>-->\r\n<!--</div>-->\r\n\r\n<!--</div>-->\r\n"

        /***/
    }),

    /***/
    "../../../../../src/app/instagram/instagram-photos.component.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return InstagramPhotosComponent;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__instagram_service__ = __webpack_require__("../../../../../src/app/instagram/instagram.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };
        var __metadata = (this && this.__metadata) || function (k, v) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
            };
        /**
         * Created by zajac on 12.02.2018.
         */



        var InstagramPhotosComponent = (function () {
            function InstagramPhotosComponent(instagramService, router) {
                this.instagramService = instagramService;
                this.router = router;
            }

            InstagramPhotosComponent.prototype.ngOnInit = function () {
                this.getInstagramImages();
            };
            InstagramPhotosComponent.prototype.getInstagramImages = function () {
                var _this = this;
                this.instagramService.getUserImages().subscribe(function (urls) {
                    _this.imagesUrls = urls.json();
                }, function (error2) {
                    console.log(error2);
                });
            };
            InstagramPhotosComponent.prototype.openSingleImage = function (url) {
                // this.router.navigate(['/link-image', url]);
                this.router.navigate(['/link-image', url]);
                // , { queryParams:  filter, skipLocationChange: true}
            };
            return InstagramPhotosComponent;
        }());
        InstagramPhotosComponent = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
                selector: 'instagramPhotos',
                template: __webpack_require__("../../../../../src/app/instagram/instagram-photos.component.html")
            }),
            __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__instagram_service__["a" /* InstagramService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__instagram_service__["a" /* InstagramService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _b || Object])
        ], InstagramPhotosComponent);

        var _a, _b;
//# sourceMappingURL=instagram-photos.component.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/instagram/instagram.service.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return InstagramService;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2__address_settings__ = __webpack_require__("../../../../../src/app/address-settings.ts");
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };
        var __metadata = (this && this.__metadata) || function (k, v) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
            };


        /**
         * Created by zajac on 12.02.2018.
         */
        var InstagramService = (function () {
            function InstagramService(http) {
                this.http = http;
                this.imagesURL = __WEBPACK_IMPORTED_MODULE_2__address_settings__["a" /* AddressSettings */].INSTAGRAM_IMAGES_URL;
            }

            InstagramService.prototype.getUserImages = function () {
                return this.http.get(this.imagesURL);
            };
            return InstagramService;
        }());
        InstagramService = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
            __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
        ], InstagramService);

        var _a;
//# sourceMappingURL=instagram.service.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/my-algorithms.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return MyAlgorithms;
        });
        /**
         * Created by zajac on 12.02.2018.
         */
        var MyAlgorithms = (function () {
            function MyAlgorithms() {
            }

            MyAlgorithms.by4Elements = function (someArray) {
                var elementsInRow = 4;
                var rows = someArray.length / elementsInRow;
                var rest = someArray.length % elementsInRow;
                rows = rows - (rest / elementsInRow);
                if (rest > 0)
                    rows = rows + 1;
                var resultArray = new Array(rows);
                var i = elementsInRow;
                var row = 0;
                for (var _i = 0, someArray_1 = someArray; _i < someArray_1.length; _i++) {
                    var idImage = someArray_1[_i];
                    if (i == 4) {
                        resultArray[row] = new Array(4);
                        row++;
                        i = 0;
                    }
                    resultArray[row - 1][i] = idImage;
                    i++;
                }
                return resultArray;
            };
            return MyAlgorithms;
        }());

//# sourceMappingURL=my-algorithms.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/navigation.component.css": /***/ (function (module, exports, __webpack_require__) {

        exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
        exports.push([module.i, "#main-menu {\r\n  padding-right: 9vw;\r\n}\r\n\r\n.login {\r\n  padding-left: 3vw;\r\n  padding-top: 0;\r\n  padding-bottom: 0;\r\n}\r\n\r\n.login-left-margin {\r\n  padding-left: 9vw;\r\n}\r\n\r\n.nav {\r\n}\r\n\r\n/*.navbar-nav {*/\r\n/*}*/\r\n/*.navbar{*/\r\n/*}*/\r\nul a {\r\n  color: #FFFFFF;\r\n}\r\n\r\n.white-text {\r\n  color: #FFFFFF;\r\n}\r\n\r\n.navbar-inverse {\r\n  font-size: 20px;\r\n  height: 55px;\r\n  background-color: #324E5C;\r\n  -webkit-border-radius: 0;\r\n}\r\n\r\n.logo {\r\n  font-size: 30px;\r\n  padding-left: 14vw;\r\n  color: #ffffff;\r\n}\r\n\r\n.button {\r\n  background-color: #47B29E;\r\n  color: #FFFFFF;\r\n  padding: 4vh 16vh 4vh 16vh;\r\n  /*border-radius: 10px;*/\r\n  /*-moz-border-radius: 10px;*/\r\n  /*-webkit-border-radius: 10px;*/\r\n  /*line-height: 20vh;*/\r\n  margin: 8vh;\r\n}\r\n\r\n.photo-property {\r\n  max-height: 50px;\r\n  margin: auto;\r\n  padding-right: 20px\r\n}\r\n", ""]);

// exports


        /*** EXPORTS FROM exports-loader ***/
        module.exports = module.exports.toString();

        /***/
    }),

    /***/
    "../../../../../src/app/navigation.component.html": /***/ (function (module, exports) {

        module.exports = "<body>\r\n<nav class=\"navbar navbar-inverse\">\r\n  <div class=\"container-fluid\">\r\n    <div class=\"navbar-header\">\r\n      <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#myNavbar\">\r\n        <span class=\"icon-bar\"></span>\r\n        <span class=\"icon-bar\"></span>\r\n        <span class=\"icon-bar\"></span>\r\n      </button>\r\n      <a class=\"navbar-brand logo\" href=\"/about\"> ONE<span class=\"week\">WEEK</span>PHOTO </a>\r\n    </div>\r\n    <div class=\"collapse navbar-collapse\" id=\"myNavbar\">\r\n      <ul class=\"nav navbar-nav navbar-right\">\r\n\r\n        <li *ngIf=\"principal.authenticated\"><a routerLink=\"/instagram\" routerLinkActive=\"active\">Instagram</a></li>\r\n        <li><a routerLink=\"/uploader\" routerLinkActive=\"active\">Upload</a></li>\r\n        <!--<li><a routerLink=\"/simpleTester\" routerLinkActive=\"active\">Tester wewnętrzny</a></li>-->\r\n\r\n        <li *ngIf=\"principal.authenticated\"><a class=\"login\" routerLink=\"/user\" routerLinkActive=\"active\">\r\n          <img\r\n            src=\"{{principal.profile_picture_url}}\"\r\n            class=\"glyphicon img-rounded photo-property\"/>\r\n          {{principal.full_name}}</a></li>\r\n        <li *ngIf=\"!principal.authenticated\"><a class=\"login-left-margin\" routerLink=\"/user\" routerLinkActive=\"active\">Profil</a>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</nav>\r\n\r\n<div class=\"container-fluid text-center\">\r\n  <div class=\"row content\" style=\"min-height: 880px\">\r\n    <div class=\"col-sm-1 sidenav\">\r\n    </div>\r\n\r\n    <div class=\"col-sm-10 text-left\">\r\n      <router-outlet></router-outlet>\r\n    </div>\r\n    <div class=\"col-sm-1 sidenav\">\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<footer class=\"container-fluid text-center\">\r\n  <br/><br/>\r\n  <p>Uploader</p>\r\n</footer>\r\n</body>\r\n"

        /***/
    }),

    /***/
    "../../../../../src/app/navigation.component.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return NavigationComponent;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__user_Principal__ = __webpack_require__("../../../../../src/app/user/Principal.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2__user_users_service__ = __webpack_require__("../../../../../src/app/user/users.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };
        var __metadata = (this && this.__metadata) || function (k, v) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
            };


        /**
         * Created by zajac on 26.02.2018.
         */
        var NavigationComponent = (function () {
            function NavigationComponent(userService, router) {
                this.userService = userService;
                this.router = router;
                this.principal = new __WEBPACK_IMPORTED_MODULE_1__user_Principal__["a" /* Principal */](false, 'undefined', 'undefined', 'undefined');
            }

            NavigationComponent.prototype.ngOnInit = function () {
                this.getUser();
            };
            NavigationComponent.prototype.getUser = function () {
                var _this = this;
                this.userService.getUser().subscribe(function (response) {
                    if (response.status <= 300) {
                        _this.principal = response.json();
                    }
                }, function (error) {
                    console.log('Unauthorized');
                });
            };
            return NavigationComponent;
        }());
        NavigationComponent = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
                selector: 'app-navigation',
                template: __webpack_require__("../../../../../src/app/navigation.component.html"),
                styles: [__webpack_require__("../../../../../src/app/navigation.component.css")]
            }),
            __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__user_users_service__["a" /* UsersService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__user_users_service__["a" /* UsersService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === "function" && _b || Object])
        ], NavigationComponent);

        var _a, _b;
//# sourceMappingURL=navigation.component.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/static/welcome.component.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return WelcomeComponent;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };

        /**
         * Created by zajac on 07.11.2017.
         */
        var WelcomeComponent = (function () {
            function WelcomeComponent() {
            }

            return WelcomeComponent;
        }());
        WelcomeComponent = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
                selector: 'welcome',
                template: __webpack_require__("../../../../../src/app/static/welcome.html"),
                styles: [__webpack_require__("../../../../../src/app/static/welcome.css")]
            })
        ], WelcomeComponent);

//# sourceMappingURL=welcome.component.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/static/welcome.css": /***/ (function (module, exports, __webpack_require__) {

        exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
        exports.push([module.i, "p {\r\n  font-size: larger;\r\n  letter-spacing: 1px;\r\n}\r\n\r\n\r\n", ""]);

// exports


        /*** EXPORTS FROM exports-loader ***/
        module.exports = module.exports.toString();

        /***/
    }),

    /***/
    "../../../../../src/app/static/welcome.html": /***/ (function (module, exports) {

        module.exports = "<br/>\r\n<div class=\"row\">\r\n  <div class=\"col-md-8\">\r\n    <h2>One Week Photo</h2>\r\n    <p><strong>Witryna przeznaczona zamieszczaniu zdjęć w chmurze</strong></p>\r\n    <p>Przechowywanie zdjęć online. Każde zdjęcie poddawane jest procesom filtracji, które\r\n      następnie może zostać pobrane lub udostępnione. Zarówno orginalna fotografia, jak i z nałożonymi filtrami, jest\r\n      dostępna pod wygenerowanym linkiem. Istnieje możliwość korzystania z konta instagram w celu identyfikacji swoich zdjęć, a także udostępniania zdjęcia bezpośrednio z Twojego konta. Wystarczy klinkąć w wybrane zdjęcie, a OneWeekPhoto pokieruje użytkownika dalej. Po uwirzytelnieniu, podczas przsyłania pojawi się opcja ukrywania przesłanych zdjęć wśród ostatnich.</p>\r\n  </div>\r\n  <div class=\"col-md-4\"></div>\r\n  <br/>\r\n  <br/>\r\n  <hr/>\r\n</div>\r\n\r\n\r\n\r\n\r\n"

        /***/
    }),

    /***/
    "../../../../../src/app/tester/simple-test.component.html": /***/ (function (module, exports) {

        module.exports = "<div class=\"row col-md-12\">\r\n  <br/>\r\n  <h2>Test poprawności działania serwisów oraz ich funkcjonalności</h2>\r\n  <br/>\r\n  <br/>\r\n  <hr/>\r\n</div>\r\n\r\n<div class=\"row col-md-12\">\r\n  <h4 *ngIf=\"!testResults\" class=\"alert alert-danger\">\r\n    <span class=\"glyphicon glyphicon-info-sign\"></span> Problem z połączeniem do serwera\r\n  </h4>\r\n</div>\r\n<!--DONE, PROCESSING, FAILED, NOTSTARTED-->\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-md-1\"><p><strong>STATUS</strong></p></div>\r\n  <div class=\"col-md-5\"><p><strong>NAZWA</strong></p></div>\r\n  <div class=\"col-md-6\"><p><strong>OPIS</strong></p></div>\r\n</div>\r\n<div *ngFor=\"let tS of testResults\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-1\">\r\n      <p *ngIf=\"tS.status===notStartedStatus\" class=\"alert alert-info\">\r\n        <span class=\"glyphicon glyphicon-off\"></span></p>\r\n      <p *ngIf=\"tS.status===processingStatus\" class=\"alert alert-warning\">\r\n        <span class=\"glyphicon glyphicon-play\"></span></p>\r\n      <p *ngIf=\"tS.status===doneStatus\" class=\"alert alert-success\">\r\n        <span class=\"glyphicon glyphicon-ok\"></span></p>\r\n      <p *ngIf=\"tS.status===failedStatus\" class=\"alert alert-danger\">\r\n        <span class=\"\tglyphicon glyphicon-remove\"></span></p>\r\n    </div>\r\n    <div class=\"col-sm-5\">\r\n      <p> {{tS.testName}}</p>\r\n    </div>\r\n    <div class=\"col-sm-6\">\r\n      {{tS.description}}\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div *ngIf=\"testResults\" class=\"row\">\r\n  <hr/>\r\n  <div class=\"col-sm-8\"></div>\r\n  <div class=\"col-sm-4\">\r\n    <button (click)=\"startTest()\" class=\"btn btn-danger\" role=\"button\">Startuj testy</button>\r\n  </div>\r\n  <hr/>\r\n</div>\r\n<!--<h3>Legenda</h3>-->\r\n<!--<div class=\"row\">-->\r\n<!--<div class=\"col-sm-2\">-->\r\n<!--&lt;!&ndash;Tu jest znaczek&ndash;&gt;-->\r\n<!--</div>-->\r\n<!--<div class=\"col-sm-10\">-->\r\n<!--&lt;!&ndash;Tu mamy opis znaczka&ndash;&gt;-->\r\n<!--</div>-->\r\n<!--<hr/>-->\r\n<!--</div>-->\r\n"

        /***/
    }),

    /***/
    "../../../../../src/app/tester/simple-test.component.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return SimpleTestComponent;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__test_service__ = __webpack_require__("../../../../../src/app/tester/test.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };
        var __metadata = (this && this.__metadata) || function (k, v) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
            };
        /**
         * Created by zajac on 30.10.2017.
         */



        var SimpleTestComponent = (function () {
            function SimpleTestComponent(testService) {
                this.testService = testService;
                this.doneStatus = "DONE";
                this.processingStatus = "PROCESSING";
                this.failedStatus = "FAILED";
                this.notStartedStatus = "NOTSTARTED";
            }

            SimpleTestComponent.prototype.getTestResults = function () {
                var _this = this;
                this.testService.getTestResults().repeatWhen(function () {
                    return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].interval(499);
                })
                    .subscribe(function (tS) {
                        return _this.testResults = tS.json();
                    }, function (error2) {
                        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].interval(5000);
                    });
            };
            SimpleTestComponent.prototype.ngOnInit = function () {
                this.getTestResults();
            };
            SimpleTestComponent.prototype.startTest = function () {
                this.testService.startTester();
            };
            return SimpleTestComponent;
        }());
        SimpleTestComponent = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
                selector: 'simpleTester',
                template: __webpack_require__("../../../../../src/app/tester/simple-test.component.html")
            }),
            __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__test_service__["a" /* TestService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__test_service__["a" /* TestService */]) === "function" && _a || Object])
        ], SimpleTestComponent);

        var _a;
//# sourceMappingURL=simple-test.component.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/tester/test.service.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return TestService;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2__address_settings__ = __webpack_require__("../../../../../src/app/address-settings.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/add/operator/toPromise.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__);
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__);
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_retry__ = __webpack_require__("../../../../rxjs/add/operator/retry.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_retry___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_retry__);
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_repeat__ = __webpack_require__("../../../../rxjs/add/operator/repeat.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_repeat___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_repeat__);
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_repeatWhen__ = __webpack_require__("../../../../rxjs/add/operator/repeatWhen.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_repeatWhen___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_repeatWhen__);
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_retryWhen__ = __webpack_require__("../../../../rxjs/add/operator/retryWhen.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_retryWhen___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_retryWhen__);
        /**
         * Created by zajac on 30.10.2017.
         */
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };
        var __metadata = (this && this.__metadata) || function (k, v) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
            };


        var TestService = (function () {
            function TestService(http) {
                this.http = http;
                this.getTestResultURL = __WEBPACK_IMPORTED_MODULE_2__address_settings__["a" /* AddressSettings */].BACKEND_REST_ENDPOINT + '/tester/getTestResults';
                this.startTestURL = __WEBPACK_IMPORTED_MODULE_2__address_settings__["a" /* AddressSettings */].BACKEND_REST_ENDPOINT + '/tester/start';
            }

            TestService.prototype.getTestResults = function () {
                return this.http.get(this.getTestResultURL).catch(function (error) {
                    return __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__["Observable"].throw(error.json().error || 'Server error');
                });
            };
            TestService.prototype.startTester = function () {
                this.http.post(this.startTestURL, null).toPromise();
            };
            TestService.prototype.handleError = function (error) {
                __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__["Observable"].interval(5000);
                return __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__["Observable"].throw('problem with connection to backend server');
            };
            return TestService;
        }());
        TestService = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
            __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
        ], TestService);

        var _a;
//# sourceMappingURL=test.service.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/uploader/FileUrlAndStatus.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return FileUrlAndStatus;
        });
        /**
         * Created by zajac on 22.03.2018.
         */
        var FileUrlAndStatus = (function () {
            function FileUrlAndStatus(filterUrl, status) {
                this.filterUrl = filterUrl;
                this.status = status;
            }

            return FileUrlAndStatus;
        }());

//# sourceMappingURL=FileUrlAndStatus.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/uploader/image-from-link-uploader.component.css": /***/ (function (module, exports, __webpack_require__) {

        exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
        exports.push([module.i, ".button {\r\n  padding: 1.5vh 4.5vw;\r\n  font-size: 1.2vw;\r\n  margin: 2vw 0 0.5vw 0;\r\n}\r\n", ""]);

// exports


        /*** EXPORTS FROM exports-loader ***/
        module.exports = module.exports.toString();

        /***/
    }),

    /***/
    "../../../../../src/app/uploader/image-from-link-uploader.component.html": /***/ (function (module, exports) {

        module.exports = "<div class=\"row col-md-12\">\r\n  <h1>Wybrane zdjęcie</h1>\r\n  <hr/>\r\n</div>\r\n<div class=\"row\">\r\n  <div class=\"col-md-8\">\r\n    <img src=\"{{imgUrl}}\" class=\"img-responsive\">\r\n  </div>\r\n  <div class=\"col-md-4\">\r\n    <input #upload type=\"submit\" value=\"Prześlij\" class=\"button \" (click)=\"userPostImage();\"/>\r\n    <br/>\r\n    <div *ngIf=\"principal.authenticated\" class=\"form-check\">\r\n      <input ngModel=\"\" type=\"checkbox\" class=\"form-check-input\" id=\"exampleCheck1\"\r\n             [checked]=\"sendAsPublic\" (change)=\"sendAsPublic = !sendAsPublic\">\r\n      <label class=\"form-check-label\" for=\"exampleCheck1\">Wyślij jako publiczne</label>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

        /***/
    }),

    /***/
    "../../../../../src/app/uploader/image-from-link-uploader.component.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return ImageFromLinkUploaderComponent;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2__user_users_service__ = __webpack_require__("../../../../../src/app/user/users.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_3__user_Principal__ = __webpack_require__("../../../../../src/app/user/Principal.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_4__image_service__ = __webpack_require__("../../../../../src/app/uploader/image.service.ts");
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };
        var __metadata = (this && this.__metadata) || function (k, v) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
            };
        /**
         * Created by zajac on 12.02.2018.
         */





        var ImageFromLinkUploaderComponent = (function () {
            function ImageFromLinkUploaderComponent(router, route, userService, imageService) {
                this.router = router;
                this.route = route;
                this.userService = userService;
                this.imageService = imageService;
                this.successMsg = null;
                this.errorMsg = null;
                this.error = null;
                this.sendAsPublic = false;
                this.principal = new __WEBPACK_IMPORTED_MODULE_3__user_Principal__["a" /* Principal */](false, 'undefined', 'undefined', 'undefined');
                this.imgUrl = "";
            }

            ImageFromLinkUploaderComponent.prototype.ngOnInit = function () {
                var _this = this;
                this.route.params.subscribe(function (params) {
                    return _this.imgUrl = (params['imgUrl']);
                });
                this.getUser();
            };
            ImageFromLinkUploaderComponent.prototype.userPostImage = function () {
                var _this = this;
                var userName = this.principal.username;
                if (!this.principal.authenticated) {
                    this.sendAsPublic = true;
                }
                this.imageService.postFormURL(this.imgUrl, userName, (!this.sendAsPublic)).subscribe(function (response) {
                    _this.successMsg = 'Wysłano z powodzeniem!!';
                    _this.idImage = response;
                    _this.router.navigate(['/image', response]);
                }),
                    function (error) {
                        return _this.errorMsg = 'Niepowodzenie wysłania pliku';
                    },
                    function () {
                        return _this.successMsg = 'Wysłano z powodzeniem!!';
                    };
            };
            ImageFromLinkUploaderComponent.prototype.getUser = function () {
                var _this = this;
                this.userService.getUser().subscribe(function (response) {
                    if (response.status <= 300) {
                        _this.principal = response.json();
                    }
                }, function (error) {
                    console.log(error);
                });
            };
            return ImageFromLinkUploaderComponent;
        }());
        ImageFromLinkUploaderComponent = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
                selector: 'imageFromLinkUploader',
                template: __webpack_require__("../../../../../src/app/uploader/image-from-link-uploader.component.html"),
                styles: [__webpack_require__("../../../../../src/app/uploader/image-from-link-uploader.component.css")]
            }),
            __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__user_users_service__["a" /* UsersService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__user_users_service__["a" /* UsersService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__image_service__["a" /* ImageService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__image_service__["a" /* ImageService */]) === "function" && _d || Object])
        ], ImageFromLinkUploaderComponent);

        var _a, _b, _c, _d;
//# sourceMappingURL=image-from-link-uploader.component.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/uploader/image.component.css": /***/ (function (module, exports, __webpack_require__) {

        exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
        exports.push([module.i, ".link {\r\n  padding: 9px;\r\n  border-radius: 3px;\r\n  margin-bottom: 20px;\r\n  background-color: #f5f5f5;\r\n  border: 1px solid #e3e3e3;\r\n  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);\r\n}\r\n\r\n.scrollable {\r\n  max-height: 110vh;\r\n  min-height: 40vh;\r\n  overflow-y: scroll;\r\n}\r\n\r\n.full-row {\r\n    margin-left:  -15px;\r\n    margin-right: -15px;\r\n}\r\n\r\n.main-photo {\r\n    margin-left: 0;\r\n    margin-right: 0;\r\n}", ""]);

// exports


        /*** EXPORTS FROM exports-loader ***/
        module.exports = module.exports.toString();

        /***/
    }),

    /***/
    "../../../../../src/app/uploader/image.component.html": /***/ (function (module, exports) {

        module.exports = "<div class=\"row col-md-12\">\r\n  <h1>Przesłane zdjęcie</h1> <h6>ID : {{imageId}}</h6>\r\n  <hr/>\r\n</div>\r\n<div class=\"row\">\r\n  <div class=\"col-lg-8\">\r\n    <div class=\"row main-photo\">\r\n      <div *ngIf=\"imgStatus === okStatus\">\r\n        <img src=\"{{imgMainView}}\" class=\"img-responsive\">\r\n      </div>\r\n      <div *ngIf=\"imgStatus !== okStatus\">\r\n        <img src=\"/assets/not_found.png\" class=\"img-responsive\">\r\n      </div>\r\n      <hr/>\r\n    </div>\r\n\t<div *ngFor=\"let filterAndStatusGroup of filtersUrlsBy4\">\r\n      <div class=\"row full-row\">\r\n        <div class=\"col-md-3\" *ngIf=\"filterAndStatusGroup[0]\">\r\n          <div *ngIf=\"filterAndStatusGroup[0].status === okStatus\">\r\n            <img src=\"{{filterAndStatusGroup[0].filterUrl}}\" class=\"img-responsive\"\r\n                 (click)=\"swapMainImg(filterAndStatusGroup[0].filterUrl)\">\r\n          </div>\r\n          <div *ngIf=\"filterAndStatusGroup[0].status === foundStatus\">\r\n            <img src=\"/loading/Ripple-1s-200px.svg\" class=\"img-responsive\">\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-3\" *ngIf=\"filterAndStatusGroup[1]\">\r\n          <div *ngIf=\"filterAndStatusGroup[1].status === okStatus\">\r\n            <img src=\"{{filterAndStatusGroup[1].filterUrl}}\" class=\"img-responsive\"\r\n                 (click)=\"swapMainImg(filterAndStatusGroup[1].filterUrl)\">\r\n          </div>\r\n          <div *ngIf=\"filterAndStatusGroup[1].status === foundStatus\">\r\n            <img src=\"/loading/Ripple-1s-200px.svg\" class=\"img-responsive\">\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-3\" *ngIf=\"filterAndStatusGroup[2]\">\r\n          <div *ngIf=\"filterAndStatusGroup[2].status === okStatus\">\r\n            <img src=\"{{filterAndStatusGroup[2].filterUrl}}\" class=\"img-responsive\"\r\n                 (click)=\"swapMainImg(filterAndStatusGroup[2].filterUrl)\">\r\n          </div>\r\n          <div *ngIf=\"filterAndStatusGroup[2].status === foundStatus\">\r\n            <img src=\"/loading/Ripple-1s-200px.svg\" class=\"img-responsive\">\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-3\" *ngIf=\"filterAndStatusGroup[3]\">\r\n          <div *ngIf=\"filterAndStatusGroup[3].status === okStatus\">\r\n            <img src=\"{{filterAndStatusGroup[3].filterUrl}}\" class=\"img-responsive\"\r\n                 (click)=\"swapMainImg(filterAndStatusGroup[3].filterUrl)\">\r\n          </div>\r\n          <div *ngIf=\"filterAndStatusGroup[3].status === foundStatus\">\r\n            <img src=\"/loading/Ripple-1s-200px.svg\" class=\"img-responsive\">\r\n          </div>\r\n        </div>\r\n      </div>\r\n\t  <br/>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-4 scrollable\">\r\n    <div *ngIf=\"imgStatus === okStatus\">\r\n      <h3> Oryginalne zdjęcie :</h3>\r\n      <div class=\"link\">{{domainURL + imgUrl}}</div>\r\n      <a href=\"{{imgUrl}}\" role=\"button\">Pobierz</a>\r\n    </div>\r\n    <hr/>\r\n    <h3>Filtrowane :</h3>\r\n    <br/>\r\n    <div *ngFor=\"let urlWithStatus of filtersUrlsAndStatuses\">\r\n      <div *ngIf=\"urlWithStatus.status === okStatus && urlWithStatus.filterUrl!=imgUrl\">\r\n        <h4>filtr {{urlWithStatus.filterUrl.split('-')[0].substr(1)}} </h4>\r\n        <div class=\"link\">\r\n          {{domainURL + urlWithStatus.filterUrl}}\r\n        </div>\r\n        <a href=\"{{urlWithStatus.filterUrl}}\" role=\"button\">Pobierz</a>\r\n      </div>\r\n      <br/>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

        /***/
    }),

    /***/
    "../../../../../src/app/uploader/image.component.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return ImageComponent;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2__address_settings__ = __webpack_require__("../../../../../src/app/address-settings.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_3__image_service__ = __webpack_require__("../../../../../src/app/uploader/image.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_4__my_algorithms__ = __webpack_require__("../../../../../src/app/my-algorithms.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_5__FileUrlAndStatus__ = __webpack_require__("../../../../../src/app/uploader/FileUrlAndStatus.ts");
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };
        var __metadata = (this && this.__metadata) || function (k, v) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
            };
        /**
         * Created by zajac on 09.11.2017.
         */






        var ImageComponent = (function () {
            function ImageComponent(route, imageService) {
                this.route = route;
                this.imageService = imageService;
                this.domainURL = __WEBPACK_IMPORTED_MODULE_2__address_settings__["a" /* AddressSettings */].DOMAIN_URL;
                this.okStatus = "OK";
                this.foundStatus = "FOUND";
                this.notFoundStatus = "NOT FOUND";
                this.minimumRefreshTime = 6;
                this.imgStatus = this.notFoundStatus;
            }

            ImageComponent.prototype.ngOnInit = function () {
                this.getImagesFiltersAndSetUrls();
            };
            ImageComponent.prototype.getImagesFiltersAndSetUrls = function () {
                var _this = this;
                this.imageService.getAddressesOfImagesFilters().subscribe(function (urls) {
                    _this.filtersUrls = urls;
                    console.log(urls);
                    _this.route.params.subscribe(function (params) {
                        return _this.setUrls(params['id']);
                    });
                    _this.autoRefreshProcessingImages();
                });
            };
            ImageComponent.prototype.autoRefreshProcessingImages = function () {
                var _this = this;
                this.minimumRefreshTime--;
                this.checkStatuses();
                var condition = false;
                for (var _i = 0, _a = this.filtersUrlsAndStatuses; _i < _a.length; _i++) {
                    var urlAndStatus = _a[_i];
                    if (urlAndStatus.status === this.foundStatus || this.minimumRefreshTime > 0) {
                        condition = true;
                    }
                }
                if (condition === true)
                    setTimeout(function () {
                        return _this.autoRefreshProcessingImages();
                    }, 250);
            };
            // autoRefreshProcessingImages() {
            //   this.checkStatuses();
            //   if (this.binarizedImgStatus === this.foundStatus ||
            //     this.colorBinarizedImgStatus === this.foundStatus ||
            //     this.firstCheck) {
            //     setTimeout(() => this.autoRefreshProcessingImages(), 1000)
            //   }
            // }
            ImageComponent.prototype.checkStatuses = function () {
                var _this = this;
                this.imageService.checkImage(this.imgUrl).subscribe(function (resultCode) {
                    _this.imgStatus = _this.translateStatus(resultCode);
                    var _loop_1 = function (filterUrlAndStatus) {
                        _this.imageService.checkImage(filterUrlAndStatus.filterUrl).subscribe(function (resultCode) {
                            filterUrlAndStatus.status = _this.translateStatus(resultCode);
                        }, function (err) {
                            return filterUrlAndStatus.status = _this.translateStatus(err.status);
                        });
                    };
                    for (var _i = 0, _a = _this.filtersUrlsAndStatuses; _i < _a.length; _i++) {
                        var filterUrlAndStatus = _a[_i];
                        _loop_1(filterUrlAndStatus);
                    }
                }, function (err) {
                    _this.imgStatus = _this.translateStatus(err);
                });
            };
            ImageComponent.prototype.translateStatus = function (codeNumber) {
                if (codeNumber == 200) {
                    return this.okStatus;
                }
                else if (codeNumber == 302) {
                    return this.foundStatus;
                }
                else
                    return this.notFoundStatus;
            };
            ImageComponent.prototype.setUrls = function (id) {
                this.imageId = String(id);
                this.imgMainView = this.imgUrl = __WEBPACK_IMPORTED_MODULE_2__address_settings__["a" /* AddressSettings */].IMAGE_URL + this.imageId;
                this.filtersUrlsAndStatuses = [];
                this.filtersUrlsAndStatuses.push(new __WEBPACK_IMPORTED_MODULE_5__FileUrlAndStatus__["a" /* FileUrlAndStatus */](this.imgUrl, this.imgStatus));
                for (var _i = 0, _a = this.filtersUrls; _i < _a.length; _i++) {
                    var filterUrl = _a[_i];
                    var addres = __WEBPACK_IMPORTED_MODULE_2__address_settings__["a" /* AddressSettings */].BACKEND_REST_ENDPOINT + filterUrl + this.imageId;
                    this.filtersUrlsAndStatuses.push(new __WEBPACK_IMPORTED_MODULE_5__FileUrlAndStatus__["a" /* FileUrlAndStatus */](addres, this.notFoundStatus));
                }
                this.filtersUrlsBy4 = __WEBPACK_IMPORTED_MODULE_4__my_algorithms__["a" /* MyAlgorithms */].by4Elements(this.filtersUrlsAndStatuses);
            };
            ImageComponent.prototype.swapMainImg = function (url) {
                this.imgMainView = url;
            };
            return ImageComponent;
        }());
        ImageComponent = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
                selector: 'image',
                template: __webpack_require__("../../../../../src/app/uploader/image.component.html"),
                styles: [__webpack_require__("../../../../../src/app/uploader/image.component.css")]
            }),
            __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__image_service__["a" /* ImageService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__image_service__["a" /* ImageService */]) === "function" && _b || Object])
        ], ImageComponent);

        var _a, _b;
//# sourceMappingURL=image.component.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/uploader/image.service.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return ImageService;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/add/operator/toPromise.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_toPromise__);
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_4__address_settings__ = __webpack_require__("../../../../../src/app/address-settings.ts");
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };
        var __metadata = (this && this.__metadata) || function (k, v) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
            };


        /**
         * Created by zajac on 08.11.2017.
         */
        var ImageService = (function () {
            function ImageService(http) {
                this.http = http;
                this.uploadImageFileUrl = __WEBPACK_IMPORTED_MODULE_4__address_settings__["a" /* AddressSettings */].UPLOAD_FILE_URL;
                this.uploadImageUrlUrl = __WEBPACK_IMPORTED_MODULE_4__address_settings__["a" /* AddressSettings */].UPLOAD_URL_URL;
                this.myImagesUrl = __WEBPACK_IMPORTED_MODULE_4__address_settings__["a" /* AddressSettings */].MY_IMAGES_IDS;
                this.lastImagesUrl = __WEBPACK_IMPORTED_MODULE_4__address_settings__["a" /* AddressSettings */].LAST_12_IDS_IMAGES_URL;
                this.imageFiltersAddresses = __WEBPACK_IMPORTED_MODULE_4__address_settings__["a" /* AddressSettings */].IMAGES_FILTERS_ADDRESSES_URL;
            }

            ImageService.prototype.getLastImages = function () {
                return this.http.get(this.lastImagesUrl).map(function (res) {
                    return res.json();
                });
            };
            ImageService.prototype.getAddressesOfImagesFilters = function () {
                return this.http.get(this.imageFiltersAddresses).map(function (res) {
                    return res.json();
                });
            };
            ImageService.prototype.getMyImages = function (userName) {
                return this.http.get(this.myImagesUrl + userName).map(function (res) {
                    return res.json();
                });
            };
            ImageService.prototype.checkImage = function (address) {
                return this.http.get(address).map(this.extractData);
            };
            ImageService.prototype.extractData = function (res) {
                return res.status;
            };
            ImageService.prototype.postFormData = function (file, userName, isHidden) {
                var _this = this;
                return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].fromPromise(new Promise(function (resolve, reject) {
                    var formData = new FormData();
                    var xhr = new XMLHttpRequest();
                    formData.append('userName', userName);
                    formData.append('isHidden', String(isHidden));
                    formData.append("file", file, file.name);
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState === 4) {
                            if (xhr.status === 200) {
                                resolve(xhr.response);
                            }
                            else {
                                reject(xhr.response);
                            }
                        }
                    };
                    xhr.open("POST", _this.uploadImageFileUrl, true);
                    // xhr.setRequestHeader('userName', 'sda');
                    xhr.send(formData);
                }));
            };
            ImageService.prototype.postFormURL = function (fileURL, userName, isHidden) {
                var _this = this;
                return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].fromPromise(new Promise(function (resolve, reject) {
                    var formData = new FormData();
                    var xhr = new XMLHttpRequest();
                    formData.append('userName', userName);
                    formData.append('isHidden', String(isHidden));
                    formData.append("imageUrl", fileURL);
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState === 4) {
                            if (xhr.status === 200) {
                                resolve(xhr.response);
                            }
                            else {
                                reject(xhr.response);
                            }
                        }
                    };
                    xhr.open("POST", _this.uploadImageUrlUrl, true);
                    // xhr.setRequestHeader('userName', 'sda');
                    xhr.send(formData);
                }));
            };
            return ImageService;
        }());
        ImageService = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
            __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _a || Object])
        ], ImageService);

        var _a;
//# sourceMappingURL=image.service.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/uploader/uploader.component.css": /***/ (function (module, exports, __webpack_require__) {

        exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
        exports.push([module.i, "/*#filedrag {*/\r\n/*display: none;*/\r\n/*font-weight: bold;*/\r\n/*text-align: center;*/\r\n/*padding: 1em 0;*/\r\n/*margin: 1em 0;*/\r\n/*color: #555;*/\r\n/*border: 2px dashed #555;*/\r\n/*border-radius: 7px;*/\r\n/*cursor: default;*/\r\n/*}*/\r\n\r\n/*#filedrag.hover {*/\r\n/*color: #f00;*/\r\n/*border-color: #f00;*/\r\n/*border-style: solid;*/\r\n/*box-shadow: inset 0 3px 4px #888;*/\r\n/*}*/\r\n\r\n/*.inputfile {*/\r\n/*!*width: 0.1px;*!*/\r\n/*!*height: 0.1px;*!*/\r\n/*opacity: 0;*/\r\n/*overflow: hidden;*/\r\n/*position: absolute;*/\r\n/*!*z-index: -1;*!*/\r\n/*}*/\r\n\r\n/*.inputfile + label {*/\r\n/*font-size: 1.25em;*/\r\n/*font-weight: 700;*/\r\n/*color: white;*/\r\n/*background-color: black;*/\r\n/*display: inline-block;*/\r\n/*}*/\r\n\r\n/*.inputfile:focus + label,*/\r\n/*.inputfile + label:hover {*/\r\n/*background-color: red;*/\r\n/*cursor: pointer; !* \"hand\" cursor *!*/\r\n/*}*/\r\nh1 {\r\n  text-align: left;\r\n}\r\n\r\nbody {\r\n  background: rgba(0, 0, 0, 0.9);\r\n}\r\n\r\nform {\r\n  /*float: right;*/\r\n  width: 28vw;\r\n  height: 32vh;\r\n  background-color: #ececec;\r\n  -webkit-border-radius: 10px;\r\n  box-shadow: inset 0 0 1.3vh 1.3vh #e6e6e6;\r\n}\r\n\r\nform p {\r\n  text-align: center;\r\n  line-height: 29vh;\r\n  color: #999;\r\n  font-size: 1.2vW;\r\n}\r\n\r\n.last-photos {\r\n  text-align: left;\r\n}\r\n\r\nform input {\r\n  position: absolute;\r\n  width: 100%;\r\n  height: 100%;\r\n  opacity: 0;\r\n}\r\n\r\n#imageToUploadUrl {\r\n  float: left;\r\n  width: 30vw;\r\n  height: 7vh;\r\n  border: 0;\r\n  text-align: center;\r\n  background-color: #f9f9f9;\r\n  -webkit-border-radius: 10px;\r\n  /*margin: auto;*/\r\n  margin-top: 11vh;\r\n  box-shadow: inset 0 0 1.3vh 1.3vh #f3f3f3;\r\n  font-size: 1.2vW;\r\n}\r\n\r\n#imageToUploadUrl::-webkit-input-placeholder {\r\n  color: #AAA;\r\n}\r\n\r\n#imageToUploadUrl:-ms-input-placeholder {\r\n  color: #AAA;\r\n}\r\n\r\n#imageToUploadUrl::placeholder {\r\n  color: #AAA;\r\n}\r\n\r\n.center-to-form {\r\n  margin-top: 12.5vh;\r\n  text-align: center;\r\n  font-weight: bold;\r\n  font-size: 2.4vh;\r\n  color: #888;\r\n}\r\n\r\n.center {\r\n  text-align: center;\r\n}\r\n\r\n* {\r\n  box-sizing: border-box;\r\n}\r\n\r\n.button {\r\n  font-size: 1.4vW;\r\n}\r\n\r\n.form-check-label {\r\n  font-size: 0.8vW;\r\n}\r\n", ""]);

// exports


        /*** EXPORTS FROM exports-loader ***/
        module.exports = module.exports.toString();

        /***/
    }),

    /***/
    "../../../../../src/app/uploader/uploader.component.html": /***/ (function (module, exports) {

        module.exports = "<!--<h1>Wyślij zdjęcie</h1>-->\r\n<!--<br/>-->\r\n<!--<br/>-->\r\n<!--<form id='formData' #formdata method=\"post\" enctype=\"multipart/form-data\">-->\r\n\r\n<br/>\r\n<br/>\r\n<br/>\r\n<br/>\r\n<div class=\"row\">\r\n  <div class=\"col-md-5\" id=\"file\">\r\n    <!--<input class=\"inputfile\"  #data type=\"file\" name=\"file\" (change)=\"getFiles(data)\"/>-->\r\n    <!--<label for=\"file\">Choose a file</label>-->\r\n    <form id='formData' #formdata method=\"post\" enctype=\"multipart/form-data\">\r\n      <input #data type=\"file\" name=\"file\" (change)=\"getFiles(data)\">\r\n      <p>Kliknij by wybrać albo upuść</p>\r\n      <!--<button  type=\"file\" (change)=\"getFiles(data)\">Upload</button>-->\r\n    </form>\r\n  </div>\r\n  <div class=\"col-md-2 center\">\r\n    <p class=\"center-to-form\">LUB</p>\r\n  </div>\r\n  <div class=\"col-md-5 center\">\r\n    <div class=\"input-group\">\r\n      <!--<span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-link\"> </i></span><span class=\"input-group-addon\">Link do zdjęcia</span>-->\r\n      <input [(ngModel)]=\"imageToUploadUrl\" id=\"imageToUploadUrl\" type=\"text\" name=\"hyperlink\"\r\n             placeholder=\"Wklej tutaj link do zdjęcia w formacie jpg...\"/>\r\n    </div>\r\n    <!--<button #upload type=\"submit\" value=\"Wyślij\" class=\"btn btn-success \" (click)=\"postFile();\">Wyślij</button>-->\r\n  </div>\r\n</div>\r\n<br/>\r\n<div class=\"row\">\r\n  <div class=\"col-md-12 center\">\r\n    <button #upload type=\"submit\" value=\"Wyślij\" class=\"button\" (click)=\"postFile();\">Wyślij</button>\r\n\r\n    <div *ngIf=\"principal.authenticated\" class=\"form-check\">\r\n      <input ngModel=\"\" type=\"checkbox\" class=\"form-check-input\" id=\"check\"\r\n             [checked]=\"sendAsPublic\" (change)=\"sendAsPublic = !sendAsPublic\">\r\n      <label class=\"form-check-label\" for=\"check\">wyślij jako publiczne</label>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-md-12 center\">\r\n    <div>\r\n      <h4 *ngIf=\"successMsg\" class=\"text-success\"> {{successMsg}}</h4>\r\n    </div>\r\n    <div>\r\n      <h4 *ngIf=\"errorMsg\" class=\"text-danger\"> {{errorMsg}}</h4>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!--TODO:Dodać info wyjaśniające publiczne lub ukryte, w zależności od wyboru-->\r\n<br/>\r\n\r\n<div class=\"row col-md-12\">\r\n  <hr/>\r\n  <h2 class=\"last-photos\">Ostatnie nadesłane zdjęcia</h2>\r\n  <hr/>\r\n</div>\r\n\r\n<div class=\"row col-md-12 last-photos\">\r\n  <masonry>\r\n    <div class=\"grid\" data-masonry='{ \"itemSelector\": \".grid-item\", \"columnWidth\": 0}'>\r\n      <div class=\"grid-sizer\"></div>\r\n      <masonry-brick class=\"brick grid-item\" *ngFor=\"let imageId of lastImagesIds\">\r\n        <img class=\"img-rounded\" src=\"{{imagesUrl+imageId}}\" routerLink=\"{{imageDetailsUrl+ imageId}}\"\r\n             routerLinkActive=\"active\"/>\r\n      </masonry-brick>\r\n    </div>\r\n  </masonry>\r\n</div>\r\n<!--<div class=\"row \">-->\r\n<!--<masonry>-->\r\n<!--<div class=\"grid\" data-masonry='{ \"itemSelector\": \".grid-item\", \"columnWidth\": 0}'>-->\r\n<!--<div class=\"grid-sizer\"></div>-->\r\n<!--<masonry-brick class=\"brick grid-item\" *ngFor=\"let brick of bricks\"><img class=\"img-rounded\"-->\r\n<!--src=\"{{brick.title}}\"/></masonry-brick>-->\r\n<!--</div>-->\r\n<!--</masonry>-->\r\n<!--</div>-->\r\n\r\n<!--<div *ngFor=\"imageUrlgeId of idImagesGrid4x\">-->\r\n<!--<div class=\"row\">-->\r\n\r\n<!--<div class=\"col-sm-3\">-->\r\n\r\n<!--<a routerLink=\"{{imageDetailsUimageUrlgeId[0]}}\" routerLinkActive=\"active\">-->\r\n<!--<img srcimageUrlsUrimageUrlgeId[0]}}\" class=\"img-responsive img-rounded\"-->\r\n<!--style=\"max-height:200px; margin: auto;\"/>-->\r\n<!--</a>-->\r\n<!--</div>-->\r\n\r\n<!--<div *ngimageUrlgeId[1]\" class=\"col-sm-3 \">-->\r\n<!--<a routerLink=\"{{imageDetailsUimageUrlgeId[1]}}\" routerLinkActive=\"active\">-->\r\n<!--<img srcimageUrlsUrimageUrlgeId[1]}}\" class=\"img-responsive img-rounded\"-->\r\n<!--style=\"max-height:200px; margin: auto;\"/>-->\r\n<!--</a>-->\r\n<!--</div>-->\r\n\r\n<!--<div *ngimageUrlgeId[2]\" class=\"col-sm-3\">-->\r\n<!--<a routerLink=\"{{imageDetailsUimageUrlgeId[2]}}\" routerLinkActive=\"active\">-->\r\n<!--<img srcimageUrlsUrimageUrlgeId[2]}}\" class=\"img-responsive img-rounded\"-->\r\n<!--style=\"max-height:200px; margin: auto;\"/>-->\r\n<!--</a>-->\r\n<!--</div>-->\r\n\r\n<!--<div *ngimageUrlgeId[3]\" class=\"col-sm-3\">-->\r\n<!--<a routerLink=\"{{imageDetailsUimageUrlgeId[3]}}\" routerLinkActive=\"active\">-->\r\n<!--<img srcimageUrlsUrimageUrlgeId[3]}}\" class=\"img-responsive img-rounded\"-->\r\n<!--style=\"max-height:200px; margin: auto;\"/>-->\r\n<!--</a>-->\r\n<!--</div>-->\r\n<!--</div>-->\r\n<!--<hr/>-->\r\n<!--</div>-->\r\n"

        /***/
    }),

    /***/
    "../../../../../src/app/uploader/uploader.component.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return UploaderComponent;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__image_service__ = __webpack_require__("../../../../../src/app/uploader/image.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_3__address_settings__ = __webpack_require__("../../../../../src/app/address-settings.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_4__user_users_service__ = __webpack_require__("../../../../../src/app/user/users.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_5__user_Principal__ = __webpack_require__("../../../../../src/app/user/Principal.ts");
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };
        var __metadata = (this && this.__metadata) || function (k, v) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
            };
        /**
         * Created by zajac on 07.11.2017.
         */






        var UploaderComponent = (function () {
            function UploaderComponent(router, imageService, userService) {
                this.router = router;
                this.imageService = imageService;
                this.userService = userService;
                this.sendAsPublic = false;
                this.principal = new __WEBPACK_IMPORTED_MODULE_5__user_Principal__["a" /* Principal */](false, 'undefined', 'undefined', 'undefined');
                this.file = null;
                this.successMsg = null;
                this.errorMsg = null;
                this.error = null;
                this.imageToUploadUrl = "";
                this.imagesUrl = __WEBPACK_IMPORTED_MODULE_3__address_settings__["a" /* AddressSettings */].IMAGE_URL;
                this.imageDetailsUrl = __WEBPACK_IMPORTED_MODULE_3__address_settings__["a" /* AddressSettings */].IMAGE_DETAIL_URL;
            }

            UploaderComponent.prototype.ngOnInit = function () {
                this.getLastImages();
                this.getUser();
            };
            UploaderComponent.prototype.getLastImages = function () {
                var _this = this;
                this.imageService.getLastImages().subscribe(function (ids) {
                    _this.lastImagesIds = ids;
                });
            };
            UploaderComponent.prototype.getFiles = function (files) {
                var empDataFiles = files.files;
                this.file = empDataFiles[0];
            };
            UploaderComponent.prototype.postImageFromUrl = function () {
                var _this = this;
                var userName = this.principal.username;
                if (!this.principal.authenticated) {
                    this.sendAsPublic = true;
                }
                this.imageService.postFormURL(this.imageToUploadUrl, userName, (!this.sendAsPublic)).subscribe(function (response) {
                    _this.successMsg = 'Wysłano z powodzeniem!!';
                    _this.idImage = response;
                    _this.router.navigate(['/image', response]);
                }),
                    function (error) {
                        return _this.errorMsg = 'Niepowodzenie wysłania pliku';
                    },
                    function () {
                        return _this.successMsg = 'Wysłano z powodzeniem!!';
                    };
            };
            UploaderComponent.prototype.postFile = function () {
                var _this = this;
                var userName = this.principal.username;
                if (!this.principal.authenticated)
                    this.sendAsPublic = true;
                if (this.file != undefined) {
                    this.imageService.postFormData(this.file, userName, !this.sendAsPublic).subscribe(function (response) {
                        _this.successMsg = 'Wysłano z powodzeniem!!';
                        _this.idImage = response;
                        _this.router.navigate(['/image', response]);
                    }),
                        function (error) {
                            return _this.errorMsg = 'Niepowodzenie wysłania pliku';
                        },
                        function () {
                            return _this.successMsg = 'Wysłano z powodzeniem!!';
                        };
                }
                else if (this.imageToUploadUrl !== "") {
                    this.postImageFromUrl();
                }
                else {
                    this.errorMsg = 'Niepowodzenie wysłania pliku';
                }
            };
            UploaderComponent.prototype.getUser = function () {
                var _this = this;
                this.userService.getUser().subscribe(function (response) {
                    if (response.status <= 300) {
                        _this.principal = response.json();
                    }
                }, function (error) {
                    console.log(error);
                });
            };
            return UploaderComponent;
        }());
        UploaderComponent = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
                selector: 'uploader',
                template: __webpack_require__("../../../../../src/app/uploader/uploader.component.html"),
                styles: [__webpack_require__("../../../../../src/app/uploader/uploader.component.css")]
            }),
            __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__image_service__["a" /* ImageService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__image_service__["a" /* ImageService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__user_users_service__["a" /* UsersService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__user_users_service__["a" /* UsersService */]) === "function" && _c || Object])
        ], UploaderComponent);

        var _a, _b, _c;
//# sourceMappingURL=uploader.component.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/user/Principal.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return Principal;
        });
        /**
         * Created by zajac on 07.02.2018.
         */
        var Principal = (function () {
            function Principal(authenticated, username, full_name, profile_picture_url) {
                this.username = username;
                this.full_name = full_name;
                this.profile_picture_url = profile_picture_url;
                this.authenticated = authenticated;
            }

            return Principal;
        }());

//# sourceMappingURL=Principal.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/user/uploaded-by-user.component.html": /***/ (function (module, exports) {

        module.exports = "<masonry>\r\n  <div class=\"grid\" data-masonry='{ \"itemSelector\": \".grid-item\", \"columnWidth\": 0}'>\r\n    <div class=\"grid-sizer\"></div>\r\n    <masonry-brick class=\"brick grid-item\" *ngFor=\"let imageId of imagesIds\">\r\n      <img class=\"img-rounded\" src=\"{{imageUrl+imageId}}\" routerLink=\"{{imageDetailsUrl+ imageId}}\"\r\n           routerLinkActive=\"active\"/>\r\n    </masonry-brick>\r\n  </div>\r\n</masonry>\r\n"

        /***/
    }),

    /***/
    "../../../../../src/app/user/uploaded-by-user.component.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return UploadedByUserComponent;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__address_settings__ = __webpack_require__("../../../../../src/app/address-settings.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2__uploader_image_service__ = __webpack_require__("../../../../../src/app/uploader/image.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_3__users_service__ = __webpack_require__("../../../../../src/app/user/users.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_4__Principal__ = __webpack_require__("../../../../../src/app/user/Principal.ts");
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };
        var __metadata = (this && this.__metadata) || function (k, v) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
            };
        /**
         * Created by zajac on 15.02.2018.
         */





        var UploadedByUserComponent = (function () {
            function UploadedByUserComponent(imageService, userService) {
                this.imageService = imageService;
                this.userService = userService;
                this.principal = new __WEBPACK_IMPORTED_MODULE_4__Principal__["a" /* Principal */](false, 'undefined', 'undefined', 'undefined');
                this.file = null;
                this.error = null;
                this.imageUrl = __WEBPACK_IMPORTED_MODULE_1__address_settings__["a" /* AddressSettings */].IMAGE_URL;
                this.imageDetailsUrl = __WEBPACK_IMPORTED_MODULE_1__address_settings__["a" /* AddressSettings */].IMAGE_DETAIL_URL;
            }

            UploadedByUserComponent.prototype.ngOnInit = function () {
                this.getUserAndImages();
            };
            UploadedByUserComponent.prototype.getMyImages = function () {
                var _this = this;
                this.imageService.getMyImages(this.principal.username).subscribe(function (ids) {
                    _this.imagesIds = ids;
                });
            };
            UploadedByUserComponent.prototype.getUserAndImages = function () {
                var _this = this;
                this.userService.getUser().subscribe(function (response) {
                    if (response.status <= 300) {
                        _this.principal = response.json();
                        _this.getMyImages();
                    }
                }, function (error) {
                    console.log(error);
                });
            };
            return UploadedByUserComponent;
        }());
        UploadedByUserComponent = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
                selector: 'uploadedByUser',
                template: __webpack_require__("../../../../../src/app/user/uploaded-by-user.component.html")
            }),
            __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__uploader_image_service__["a" /* ImageService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__uploader_image_service__["a" /* ImageService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__users_service__["a" /* UsersService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__users_service__["a" /* UsersService */]) === "function" && _b || Object])
        ], UploadedByUserComponent);

        var _a, _b;
//# sourceMappingURL=uploaded-by-user.component.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/user/users.component.css": /***/ (function (module, exports, __webpack_require__) {

        exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
        exports.push([module.i, ".gray {\r\n  background-color: #CEC;\r\n}\r\n\r\n.jumbotron {\r\n  padding-right: 15px;\r\n  padding-left: 15px;\r\n}\r\n\r\n.btn {\r\n  border: 0;\r\n  padding: 1vh 3vw;\r\n  font-size: 1vw;\r\n  margin-top: 8px;\r\n  margin-bottom: 12px;\r\n  -webkit-border-radius: 0;\r\n  cursor: pointer;\r\n}\r\n\r\n.image-size {\r\n  height: 28vh;\r\n}\r\n", ""]);

// exports


        /*** EXPORTS FROM exports-loader ***/
        module.exports = module.exports.toString();

        /***/
    }),

    /***/
    "../../../../../src/app/user/users.component.html": /***/ (function (module, exports) {

        module.exports = "<div class=\"row jumbotron\" *ngIf=\"!principal.authenticated\">\n  <h3>Witaj nieznajomy</h3>\n  <div>\n    <a class=\"LogIn\" href=\"{{this.instagramLoginLink}}\"> Zaloguj się za pomocą instagrama, by móc w pełni korzystać z\n      serwisu. </a>\n  </div>\n</div>\n<div class=\" row jumbotron\" *ngIf=\"principal.authenticated\">\n  <!--<div >-->\n  <div class=\"col-md-3\">\n    <!--class=\"glyphicon img-rounded\"-->\n    <img src=\"{{principal.profile_picture_url}}\" class=\"img-rounded image-size\"/>\n  </div>\n  <div class=\"col-md-9\">\n    <h3>Profil</h3>\n    <h3><strong>Godność:</strong> {{principal.full_name}}</h3>\n    <h3><strong>Nazwa użytkownika:</strong> {{principal.username}}</h3>\n    <a (click)=\"logOut()\" class=\"btn btn-danger\" role=\"button\">Wyloguj</a>\n  </div>\n</div>\n<div class=\"row col-md-12\" *ngIf=\"principal.authenticated\">\n  <hr/>\n  <h2>Twój magazyn</h2>\n  <hr/>\n  <uploadedByUser></uploadedByUser>\n</div>\n\n"

        /***/
    }),

    /***/
    "../../../../../src/app/user/users.component.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return UserComponent;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__users_service__ = __webpack_require__("../../../../../src/app/user/users.service.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2__Principal__ = __webpack_require__("../../../../../src/app/user/Principal.ts");
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };
        var __metadata = (this && this.__metadata) || function (k, v) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
            };


        var UserComponent = (function () {
            function UserComponent(userService) {
                this.userService = userService;
                this.principal = new __WEBPACK_IMPORTED_MODULE_2__Principal__["a" /* Principal */](false, 'undefined', 'undefined', 'undefined');
                // principal: Principal = new Principal(false, 'ADamzajacas', 'Adam undefined', 'https://scontent-frx5-1.cdninstagram.com/vp/3989ec07b89b98ff92579a3c2e022eac/5B2A81B5/t51.2885-19/s150x150/24327328_1526892724058489_7935518903373922304_n.jpg');
                this.instagramLoginLink = "/oauth/login/instagram";
            }

            UserComponent.prototype.ngOnInit = function () {
                var _this = this;
                setTimeout(function () {
                    _this.getUser();
                }, 250);
            };
            UserComponent.prototype.getUser = function () {
                var _this = this;
                this.userService.getUser().subscribe(function (response) {
                    if (response.status <= 300) {
                        _this.principal = response.json();
                    }
                }, function (error) {
                    console.log(error);
                });
            };
            UserComponent.prototype.logOut = function () {
                console.log('logout');
                this.userService.logout().subscribe(function (res) {
                    return console.log(res.json());
                }, function (error2) {
                    return console.log(error2.json());
                });
                window.location.href = "/";
            };
            return UserComponent;
        }());
        UserComponent = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
                selector: 'user',
                template: __webpack_require__("../../../../../src/app/user/users.component.html"),
                styles: [__webpack_require__("../../../../../src/app/user/users.component.css")],
                providers: [__WEBPACK_IMPORTED_MODULE_1__users_service__["a" /* UsersService */]]
            }),
            __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__users_service__["a" /* UsersService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__users_service__["a" /* UsersService */]) === "function" && _a || Object])
        ], UserComponent);

        var _a;
//# sourceMappingURL=users.component.js.map

        /***/
    }),

    /***/
    "../../../../../src/app/user/users.service.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return UsersService;
        });
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2_ng2_cookies_ng2_cookies__ = __webpack_require__("../../../../ng2-cookies/ng2-cookies.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2_ng2_cookies_ng2_cookies___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng2_cookies_ng2_cookies__);
        var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
                var c = arguments.length,
                    r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            };
        var __metadata = (this && this.__metadata) || function (k, v) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
            };


        /**
         * Created by zajac on 29.01.2018.
         */
        var UsersService = (function () {
            function UsersService(http) {
                this.http = http;
                this.usersURL = '/oauth/user/self'; // URL to web api
                this.logoutURL = '/oauth/logout'; // URL to web api
            }

            UsersService.prototype.getUser = function () {
                return this.http.get(this.usersURL, this.makeOptions());
            };
            UsersService.prototype.logout = function () {
                __WEBPACK_IMPORTED_MODULE_2_ng2_cookies_ng2_cookies__["Cookie"].deleteAll();
                return this.http.post(this.logoutURL, '', this.makeOptions());
            };
            // private handleError(error: any): Promise<any> {
            //   return Promise.reject('User not logged in.');
            // }
            UsersService.prototype.makeOptions = function () {
                var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({'Content-Type': 'application/json'});
                //     'X-XSRF-TOKEN': Cookie.get('XSRF-TOKEN')
                return new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({headers: headers});
            };
            return UsersService;
        }());
        UsersService = __decorate([
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
            __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
        ], UsersService);

        var _a;
//# sourceMappingURL=users.service.js.map

        /***/
    }),

    /***/
    "../../../../../src/assets/background.jpg": /***/ (function (module, exports, __webpack_require__) {

        module.exports = __webpack_require__.p + "background.e4793102d1936ebf823e.jpg";

        /***/
    }),

    /***/
    "../../../../../src/environments/environment.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        /* harmony export (binding) */
        __webpack_require__.d(__webpack_exports__, "a", function () {
            return environment;
        });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
        var environment = {
            production: false
        };
//# sourceMappingURL=environment.js.map

        /***/
    }),

    /***/
    "../../../../../src/main.ts": /***/ (function (module, __webpack_exports__, __webpack_require__) {

        "use strict";
        Object.defineProperty(__webpack_exports__, "__esModule", {value: true});
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
        /* harmony import */
        var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");


        if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
            Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_23" /* enableProdMode */])();
        }
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
            .catch(function (err) {
                return console.log(err);
            });
//# sourceMappingURL=main.js.map

        /***/
    }),

    /***/
    0: /***/ (function (module, exports, __webpack_require__) {

        module.exports = __webpack_require__("../../../../../src/main.ts");


        /***/
    })

}, [0]);
//# sourceMappingURL=main.bundle.js.map